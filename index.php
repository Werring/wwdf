<?php
	/**
	 * Index file
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Core
	 * @copyright 2009-2012 Werring webdevelopment
	 */


	/**
	 * Direct link to the base path of the project
	 *
	 * @package Core
	 */
	define('__PATH__', dirname(__FILE__) . DIRECTORY_SEPARATOR);
	/**
	 * Autoloader
	 *
	 * @param string $className
	 */
	function __autoload($className){
		$classPath = "classes/" . (str_replace("_", "/", $className) . ".php");
		if(file_exists($classPath)){
			require_once $classPath;
		}

	}

	$main = new WWDF();
	if(Registry::getInstance()->Conf->md5Integrity != md5_file(__FILE__)) Model_Logfile::writeLog('ERROR', 'MD5 integrity check failed!', 1);
	$main->init();
/**
 * WWDF end
 */