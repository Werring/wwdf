<?php
	/**
	 * Registry singleton class
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Core
	 * @copyright 2009-2012 Werring webdevelopment
	 *
	 * @property Model_Request       $Request   Model_Request
	 * @property Model_Database      $DB        Model_Database
	 * @property Model_Configuration $Conf      Model_Configuration
	 * @property View_JSON           $JSON      View_JSON
	 * @property View_Template       $Template  View_Template
	 * @property Dispatcher          $Dispatch  Dispatcher
	 * @property Model_User          $User      Model_User
	 * @property Model_Header        $Headers   Model_Header
	 * @property Model_Menu			 $Menu		Model_Menu
	 */
	final class Registry extends stdClass {

		/**
		 * Singleton instance of self
		 *
		 * @var Registry $instance
		 * @static
		 */
		private static $instance = null;

		/**
		 * Make sure we cant call this function outside itself (Singleton)
		 *
		 * @access private
		 * @final
		 */
		private final function __construct(){ }

		/**
		 * Get an instance of Registry
		 *
		 * @static
		 * @return Registry
		 */
		public static function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new Registry();
			}
			return self::$instance;
		}
	}
