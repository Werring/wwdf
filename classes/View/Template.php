<?php

	/**
	 * Simple Template Engine
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   View
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	final class View_Template extends View_Base {
		const begTMPL = '${';
		const endTMPL = '}';
		/**
		 * Template Name
		 *
		 * @var string
		 * @access private
		 */
		private $tmplName = null;
		/**
		 * Template Location
		 *
		 * @var string
		 * @access private
		 */
		private $tmplLoc = null;
		/**
		 * Template Content
		 *
		 * @var array
		 * @access private
		 */
		private $tmplContent = array();
		/**
		 * Template Variables
		 *
		 * @var array
		 * @access private
		 */
		private $tmplVars = array();
		/**
		 * Template Variables with wildcards
		 *
		 * @var array
		 * @access private
		 */
		private $tmplWildVars = array();
		/**
		 * Template Blocks
		 *
		 * @var array
		 * @access private
		 */
		private $tmplBlocks = array();

		/**
		 * Javascript embed lines
		 *
		 * @var string
		 * @access private
		 */
		private $JS = "";
		/**
		 * CSS embed lines
		 *
		 * @var string
		 * @access private
		 */
		private $CSS = "";

		/**
		 * Web Path
		 *
		 * @var string
		 * @access private
		 */
		private $webPath = "/";

		/**
		 * Constructor
		 *
		 * @param string $name
		 *
		 * @access public
		 */
		public function __construct($name){
			$this->initView();
			$this->setTemplate($name);
			$this->setVar('template', $name);
		}

		/**
		 * Set template
		 *
		 * Search for given template and loads it
		 *
		 * @param string $name
		 *
		 * @access public
		 */
		public function setTemplate($name){

			if(file_exists(__PATH__ . 'templates/' . $name . '/main.html')){
				$this->tmplName = $name;
				$this->tmplLoc  = __PATH__ . 'templates/' . $name . '/';
				$this->updateWebPath();
				$this->tmplContent = $this->importFiles($this->tmplLoc);
			}
			else
			{
				$this->output = "no " . __PATH__ . 'templates/' . $name . '/main.html';
				Model_Logfile::writeLog('Template', $this->output, 2);
			}

		}

		/**
		 * Imports all .html files in template directory ans stores them as template parameters
		 *
		 * @access private
		 *
		 * @param $dir
		 *
		 * @return array
		 */
		private function importFiles($dir){
			$dirList  = scandir($dir);
			$retArray = array();
			for($i = 0; $i < count($dirList); $i++)
			{
				if(is_dir($dir . $dirList[$i])) continue;
				$pInfo = pathinfo($dirList[$i]);
				if($pInfo['extension'] == 'html'){
					$retArray[$pInfo['filename']] = file_get_contents($dir . $dirList[$i]);
				}
			}
			return $retArray;
		}

		/**
		 * Set template var so it can be replaced in the template
		 *
		 * @access public
		 *
		 * @param $name
		 * @param $value
		 */
		public function setVar($name, $value){
			$this->tmplVars[$name] = $value;
		}

		/**
		 * Set template wildcard var so it can be replaced in the template
		 *
		 * Allowed wildcards are:
		 * - * (0 or more non space chars)
		 * - ? (0 or 1    non space char )
		 *
		 * @access public
		 *
		 * @param $name
		 * @param $value
		 */
		public function setWildVar($name, $value){
			$this->tmplWildVars[$name] = $value;
		}

		/**
		 * Set assoc array to template vars
		 *
		 * @access public
		 *
		 * @param $templateVars
		 */
		public function fillTemplate($templateVars){
			if(is_array($templateVars)){
				foreach($templateVars as $key => $value)
				{
					$this->setVar($key, $value);
				}
			}
		}

		/**
		 * Parse given template
		 *
		 * @access public
		 *
		 * @param string $tmpl
		 * @param bool   $return
		 *
		 * @return mixed
		 */
		public function parse($tmpl = null, $return = false){
			if(is_null($tmpl)){
				$this->reg->Menu->renderMenu();
				foreach($this->tmplContent as $template => $val)
				{
					if($template == 'main') continue;
					if(!isset($this->tmplVars[$template])){
						$this->setVar($template, $this->parse($template, true));
					}
				}
				$this->parse('main');
			}
			else
			{
				$this->setVar('JSCCS', $this->JS . PHP_EOL . $this->CSS);

				$html = $this->tmplContent[$tmpl];
				$this->renderHTML($html);
				if($return){
					return $html;
				}
				else
				{
					$this->output = $html;
				}
				Model_Logfile::writeLog('Template', 'Template: ' . $tmpl);
			}
			return null;
		}

		/**
		 * Import page for embedding inside template
		 *
		 * @access public
		 *
		 * @param string      $path Path to file
		 * @param string|null $name tmplVar name
		 * @param array       $replaceVars
		 * @param bool        $rel  Relative to template directory
		 */
		public function addPage($path, $name = null, $replaceVars = array(), $rel = true){
			if(!$rel) $filePath = $path;
			else $filePath = $this->tmplLoc . $path;
			if(file_exists($filePath)){
				$pInfo = pathinfo($filePath);
				if($pInfo['extension'] == 'html'){
					$name                     = is_null($name) ? $pInfo['filename'] : $name;
					$this->tmplContent[$name] = file_get_contents($filePath);
					foreach($replaceVars as $key => $value){
						str_replace(self::begTMPL . $key . self::endTMPL, $value, $this->tmplContent[$name]);
					}
					unset($this->tmplVars[$name]);
				}
			}
		}


		/**
		 * Import block and fill it with the given parameters
		 *
		 * @access public
		 *
		 * @param string      $file Path to file
		 * @param string|null $name tmplVar name
		 * @param array       $blockVars
		 */
		public function addBlock($file, $name, $blockVars){
			$filePath = $this->tmplLoc . 'htmlblocks/' . $file . '.html';
			if(file_exists($filePath)){
				$pInfo = pathinfo($filePath);
				if($pInfo['extension'] == 'html'){
					$name  = is_null($name) ? $pInfo['filename'] : $name;
					$block = file_get_contents($filePath);
					$blocks = array();
					foreach($blockVars as $var){
						$html = $block;
						foreach($var as $key => $value)
						{
							$html = str_replace(self::begTMPL . $key . self::endTMPL, $value, $html);
						}
						$blocks[] = $html;
					}
					$this->tmplBlocks[$name] = $blocks;
				}
			}
		}

		/**
		 * Render the HTML
		 *
		 * @access protected
		 *
		 * @param $html
		 */
		protected function renderHTML(&$html){
			foreach($this->tmplBlocks as $key => $block){
				$tag   = self::begTMPL . '[' . $key . ']' . self::endTMPL;
				$value = '';
				for($i = 0; $i < count($block); $i++){
					$value .= $block[$i] . PHP_EOL;
				}
				$html = str_replace($tag, $value, $html);
			}
			foreach($this->tmplVars as $key => $value)
			{
				$html = str_replace(self::begTMPL . $key . self::endTMPL, $value, $html);
				$this->setWildVar($key . ':*', $value);
			}
			foreach($this->tmplWildVars as $key=> $value){

				$regex = '/' . preg_quote(self::begTMPL, '/') . str_replace(array("\?\*", "\*", '\?'), array('.\S+', '\S*', '\S'), preg_quote($key, '/')) . preg_quote(self::endTMPL, '/') . '/';

				$html = preg_replace($regex, $value, $html);
			}
		}

		/**
		 * Add CSS file to template
		 *
		 * @access public
		 *
		 * @param string $filePath
		 * @param bool   $tmplDir
		 */
		public function addCSS($filePath, $tmplDir = true){
			$filePath = ($tmplDir) ? $this->webPath . $filePath : $filePath;
			$this->CSS .= "<link type=\"text/css\" media=\"screen\" rel=\"stylesheet\" href=\"{$filePath}\" />" . PHP_EOL;
		}

		/**
		 * Add JS file to template
		 *
		 * @access public
		 *
		 * @param string $filePath
		 * @param bool   $tmplDir
		 */
		public function addJS($filePath, $tmplDir = true){
			$filePath = ($tmplDir) ? $this->webPath() . $filePath : $filePath;
			$this->JS .= "<script type=\"text/javascript\" src='{$filePath}' ></script>" . PHP_EOL;
		}

		/**
		 * Delete all JS files added with addJS
		 *
		 * @access public
		 * @see    addJS
		 */
		public function deleteJs(){
			$this->JS = "";
		}

		/**
		 * Delete all CSS files added with addCSS
		 *
		 * @access public
		 * @see    addCSS
		 */
		public function deleteCss(){
			$this->CSS = "";
		}

		/**
		 * Wrap value with HTML tag with given attributes (string or assoc array)
		 *
		 * @access public
		 *
		 * @param string       $tag
		 * @param string       $string
		 * @param string|array $attr
		 *
		 * @return string
		 */
		public function wrapTag($tag, $string, $attr = ""){
			if(is_array($attr)){
				$attributes = "";
				foreach($attr as $key=> $value)
				{
					$attributes .= $key . "='" . $value . "' ";
				}
			}
			else
			{
				$attributes = $attr;
			}

			return "<" . $tag . " " . $attributes . ">" . $string . "</" . $tag . ">";
		}

		/**
		 * Build a template var
		 *
		 * @acces public
		 *
		 * @param string $name
		 *
		 * @return string
		 */
		public function templateVar($name){
			return self::begTMPL . $name . self::endTMPL;
		}

		/**
		 * Update webPath
		 *
		 * @access protected
		 */
		protected function updateWebPath(){
			$this->webPath = str_replace(__PATH__, "/", $this->tmplLoc);
		}

		/**
		 * Get webPath
		 *
		 * @access public
		 * @return string
		 */
		public function webPath(){
			return $this->webPath;
		}
	}
