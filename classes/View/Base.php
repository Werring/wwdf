<?php
	/**
	 * Base view
	 *
	 * Can not be called directly, has to be extended
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   View
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class View_Base {
		/**
		 * Link to registry
		 *
		 * @access protected
		 * @var Registry
		 */
		protected $reg;


		/**
		 * Data to output
		 *
		 * @access protected
		 * @var string
		 */
		protected $output = "";

		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			$this->initView();
		}

		/**
		 * Destructor
		 *
		 * Parse output when detructed
		 *
		 * @access public
		 */
		public function __destruct(){
			echo $this->output;
		}

		/**
		 * Initialize view
		 *
		 * @access protected
		 * @final
		 */
		protected final function  initView(){
			$this->reg = Registry::getInstance();
		}
	}
