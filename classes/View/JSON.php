<?php
	/**
	 * View_JSON
	 *
	 * <p>Outputs JSON to current view and exits the script</p>
	 * <p>Usefull for AJAX queries</p>
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   View
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class View_JSON extends View_Base {

		/**
		 * Array that will become the JSON output
		 *
		 * @access private
		 * @var array
		 */
		private $jsonData;

		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			$this->initView();
		}

		/**
		 * Add data to JSON
		 *
		 * <p>Add data to JSON class, data can be a string or assoc array</p>
		 * <p>If assoc array is given, name should be null</p>
		 *
		 * @access public
		 *
		 * @param array|string $data
		 * @param string|null  $name
		 * @param bool         $overwrite
		 *
		 * @return bool
		 */
		public function add($data, $name = null, $overwrite = false){
			$isOk = true;
			if(is_array($data) && is_null($name)){
				foreach($data as $key => $value)
				{

					if(!$isOk) break;
					$isOk = $this->add($key, $value, $overwrite);
				}
			}

			if(isset($this->jsonData[$name]) && $overwrite === false) $isOk = false;
			else $this->jsonData[$name] = $data;
			return $isOk;
		}

		/**
		 * Parse JSON and calls die()
		 *
		 * @access public
		 */
		public function parse(){
			Model_Logfile::writeLog('JSON', 'calls DIE');
			$this->reg->Headers->set('Content-Type', 'application/json');
			$this->output = json_encode($this->jsonData);
			die;
		}

	}
