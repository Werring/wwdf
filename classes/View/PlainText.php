<?php
	/**
	 * View_PlainText
	 * <p>Display content as text/plain</p>
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   View
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class View_PlainText extends View_Base {
		/**
		 * Constructor sends file to correct method
		 *
		 * @access public
		 */
		public function __construct(){
			$this->initView();
			$args = (func_num_args()) ? func_get_arg(0) : null;
			if($args['output']){
				$this->output = $args['output'];
			}
			$this->reg->Headers->set('Content-Type','text/plain');
			die;
		}

		/**
		 * Output given text
		 *
		 * @access public
		 * @static
		 *
		 * @param $output
		 */
		public static function renderText($output){
			$arg['output'] = $output;
			new View_PlainText($arg);
		}
	}
