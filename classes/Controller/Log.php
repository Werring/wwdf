<?php
	/**
	 * Controller for the Log views
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Controller
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Controller_Log extends Controller_Base {
		/**
		 * Redirects request to view the requested logfile
		 *
		 * @access public
		 */
		public function __construct(){
			$this->arguments = (func_num_args()) ? func_get_arg(0) : null;
			$this->initControler();
			$log = new Model_Log($this->arguments['filename']);
			View_PlainText::renderText($log);
		}
	}