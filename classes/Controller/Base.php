<?php
	/**
	 * Controller Base is extended by all Controllers
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Controller
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Controller_Base {
		/**
		 * Link to the Registery
		 *
		 * @var Registry
		 * @access protected
		 */
		protected $reg;

		/**
		 * Arguments passed through this page
		 *
		 * @var array
		 * @access protected
		 */
		protected $arguments;

		/**
		 * Set Registery to $reg
		 *
		 * @access protected
		 */
		protected final function  initControler(){
			$this->reg = Registry::getInstance();
		}

	}
