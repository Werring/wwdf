<?php
	/**
	 * Controler_Page
	 *
	 * Does not create pages by it self, but it rather creates functions and lays a base for other pages<br/>
	 * When called directly, it tries to execute a Controller_Page
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Controller
	 * @subpackage Pages
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class  Controller_Page extends Controller_Base {
		/**
		 * Check what Controller should handle given page and calls that controller
		 *
		 * @access public
		 */
		public function __construct(){

			$this->arguments = (func_num_args()) ? func_get_arg(0) : null;
			if(is_string($this->arguments['param'])){
				new Controller_Page_Database($this->arguments);
			}
			else{
				if($this->arguments['param'][0] == 'd'){
					$this->arguments['param'][0] = 'database';
				}
				$class = 'Controller_Page' . ucfirst($this->arguments['param'][0]);
				if(class_exists($class)){
					new $class($this->arguments);
				}
				else{
					$this->initControler();
					Controller_Page_404::create404($this->reg->Dispatch->page);
				}
			}
		}

		/**
		 * Set some basic page elements
		 *
		 * Set menu, templatePath and calls initController
		 *
		 * @see    Controller_Base::initController
		 *
		 * @access proteced
		 * @final
		 */
		protected final function initPage(){
			$this->initControler();
			if($this->reg->Dispatch->page == ""){
				$this->reg->Headers->set('Location', '/home');
				exit;
			}
			if(isset($this->arguments['param'])){
				if(!is_array($this->arguments['param'])) $this->reg->Template->setVar('header', $this->arguments['param']);
				else $this->reg->Template->setVar('header', var_export($this->arguments['param'], true));
			}
			$this->reg->Template->setVar('templatePath', $this->reg->Template->webPath());
			$menu = $this->reg->Menu;
			$menu->addMenuItem('Home','/home');
			$menu->getPagesFromDB();
			$menu->addMenuItem('PHP docs','/docs','_blank');
		}

		/**
		 * Check is user is logged in
		 *
		 * If user is logged in, call the method given with $for<br/>
		 * If user is not logged in and $loginForm is true, display login form<br/>
		 * Else just display a message stating the user needs to be logged in<br/>
		 *
		 * @access protected
		 *
		 * @param string $for       existing Method in $this class
		 * @param bool   $loginForm display a login form
		 */
		protected function loginRequired($for = "", $loginForm = false){
			if(!$this->reg->User->loggedIn && $loginForm){
				$loginForm = new Model_Form('login', '/login', 'POST');
				$loginForm->startFieldSet('Login', 'Log in');
				$loginForm->addInputField('username', false);
				$loginForm->addInputField('password', false, '', array('type'=> 'password'));
				$loginForm->addBox('Keep session', 'SaveSession', array(array('text' => '',
				                                                              'value'=> 'y')), 'checkbox', 1, false);
				$loginForm->addButton('login', 'submit', 'Verstuur', array('value'=> 'Log in'));
				$this->reg->Template->setVar('content', $loginForm);
				$this->reg->Template->setVar('menu:class:' . $this->reg->Dispatch->page, '');
				$this->reg->Template->setVar('menu:class:login', 'selected');

			}
			elseif(!$this->reg->User->loggedIn && !$loginForm){
				$this->reg->Headers->setHttpStatusResponse(401);
				$content = <<<HTML
<h3>Login Required</h3>
<p>You need te be logged in to view this page,<br/> please <a href="/login">log in</a> to continue.</p>
HTML;

				$this->reg->Template->setVar('content', $content);

			}
			elseif(method_exists($this, $for) && $this->reg->User->loggedIn){
				call_user_func(array($this, $for));
			}
		}
	}
