<?php
	/**
	 * Handles plaintext pages like robots.txt and humans.txt
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Controller
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Controller_Plaintext extends Controller_Base {
		/**
		 * Checks if we should call robots.txt or humans.txt
		 *
		 * @access public
		 */
		public function __construct(){
			$this->initControler();
			switch($this->reg->Dispatch->page){
				case 'humans.txt':
					$this->humans();
					break;
				case 'robots.txt':
					$this->robots();
					break;
			}
		}

		/**
		 * Shows robots.txt
		 *
		 * Disallowing browsing in /classes/ /logs/ /docs/ and /templates/
		 *
		 * @access private
		 */
		private function robots(){
			$output = <<<TXT
user-agent: *
	Disallow: /classes/
	Disallow: /logs/
	Disallow: /templates/
	Disallow: /docs/
TXT;
			View_PlainText::renderText($output);
		}

		/**
		 * Shows humans.txt
		 *
		 * Displays info about site builders
		 *
		 * @access private
		 */
		private function humans(){
			$output = <<<TXT
/* WWDF TEAM */
Developer: Thom Werring
Contact: <info AT werringweb DOT nl>
Twitter: @werwebdev
Location: Beemster, Holland

/* SITE */
Last update: March 2012
Standards: php 5
Software: PHPStorm
TXT;
			View_PlainText::renderText($output);
		}
	}
