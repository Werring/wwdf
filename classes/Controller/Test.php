<?php
	/**
	 * Testing View class
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   View
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Controller_Test extends Controller_Base {
		/**
		 * Dumps all arguments with serialize
		 */
		public function __construct(){
			$args        = func_get_args();
			$this->initControler();
			$text =var_export(getallheaders(),true) . PHP_EOL . var_export($_SERVER,true);
			View_PlainText::renderText($text);
		}
	}
