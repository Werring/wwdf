<?php
	/**
	 * Handles 404 pages
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Controller
	 * @subpackage Pages
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class Controller_Page_404 extends Controller_Page_Error {
		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			$this->arguments           = (func_num_args()) ? func_get_arg(0) : null;
			$this->arguments['source'] = is_array($this->arguments['source']) ? implode('/', $this->arguments['source']) : $this->arguments['source'];
			$this->init404();
		}

		/**
		 * Loads 404 Error page
		 *
		 * Sets 404header<br/>
		 * Loads 404 Error page<br/>
		 * Fills in template vars<br/>
		 *
		 * @access public
		 */
		public function init404(){
			$this->initError();
			$this->reg->Headers->setHttpStatusResponse(404);
			$this->reg->Template->addCSS('errorpages/style/404.css');
			$this->reg->Template->addPage('errorpages/404.html');
			$templateVars            = array();
			$templateVars['content'] = $this->reg->Template->templateVar('404');
			$templateVars['header']  = 'Oh no! A 404 Error';
			$templateVars['404URL']  = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $this->arguments['source'];
			$templateVars['404Path'] = $this->arguments['source'];
			$this->reg->Template->fillTemplate($templateVars);
		}

		/**
		 * Redirects user to a 404 page
		 *
		 * @static
		 * @access public
		 *
		 * @param string $from
		 */
		public static function create404($from){
			Registry::getInstance()->Headers->set('Location','/error/404/' . $from);
			exit;
		}
	}
