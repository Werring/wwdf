<?php
	/**
	 * Basic error controller
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Controller
	 * @subpackage Pages
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class Controller_Page_Error extends Controller_Page {
		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			$this->arguments = (func_num_args()) ? func_get_arg(0) : null;
			$this->initError();
		}

		/**
		 * Initializer
		 *
		 * Sets errorPagePath as template variable
		 *
		 * @access protected
		 */
		protected function initError(){
			$this->initPage();
			$this->reg->Template->setVar('errorPagePath', $this->reg->Template->webPath() . 'errorpages/');
		}
	}
