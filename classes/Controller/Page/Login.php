<?php
	/**
	 * Login Controller
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Controller
	 * @subpackage Pages
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class Controller_Page_Login extends Controller_Page {
		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			$this->arguments = (func_num_args()) ? func_get_arg(0) : null;
			$this->initPage();
			$this->handleLogin();
		}

		/**
		 * Handle Login and Logout
		 *
		 * @access protected
		 */
		protected function handleLogin(){
			$this->reg->Template->setVar('name', $this->reg->Dispatch->page);
			switch($this->reg->Dispatch->page)
			{
				case 'login':
					$this->reg->Template->setVar('header', 'log in');
					$this->loginRequired('redirectToSource', true);
					$this->reg->User->logIn();

					break;
				case 'logout':
					if($this->reg->User->loggedIn){
						$this->reg->Template->setVar('header', 'log out');
						$this->reg->User->logOut();
						$this->reg->Template->setVar('content', 'You have been logged out.');
					}
					else{
						$this->redirectToSource('/login');
					}
					break;
			}

		}

		/**
		 * Redirect to referer, /home or $path
		 *
		 * @param bool|string $path
		 *
		 * @access protected
		 */
		protected function redirectToSource($path = false){
			$tmp = explode($_SERVER['HTTP_HOST'], $_SERVER['HTTP_REFERER']);
			if(count($tmp) == 2 && !$path && $tmp[1] != '/login'){
				$source = $tmp[1];
			}
			elseif($path === false){
				$source = '/home';
			}
			else{
				$source = $path;
			}
			$this->reg->Headers->set('Location', $source);
			Model_Logfile::writeLog('Login', 'Redirecting to ' . $source, 10);
			exit;
		}
	}
