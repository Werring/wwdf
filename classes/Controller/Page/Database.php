<?php
	/**
	 * Controller for pages from the database
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Controller
	 * @subpackage Pages
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class Controller_Page_Database extends Controller_Page {

		/**
		 * Current Page
		 *
		 * @var string
		 * @access protected
		 */
		protected $page;
		/**
		 * Link to database page model
		 *
		 * @var Model_DatabasePage
		 * @access protected
		 */
		protected $pageModel;

		/**
		 * Constructor gets data for this paeg and sends them to template
		 *
		 * @access public
		 */
		public function __construct(){
			$this->arguments = (func_num_args()) ? func_get_arg(0) : null;
			$this->initPage();
			if(is_array($this->arguments['param']) && count($this->arguments['param']) == 1){
				$this->page = $this->arguments['param'][0];
			}
			elseif(is_array($this->arguments['param']) && $this->arguments['param'][0] == 'database'){
				unset($this->arguments['param'][0]);
				$this->page = implode('/', $this->arguments['param']);
			}
			elseif(is_array($this->arguments['param'])){
				$this->page = implode('/', $this->arguments['param']);
			}
			else{
				$this->page = $this->arguments['param'];
			}
			$this->pageModel = new Model_DatabasePage($this->page);


			if($this->pageModel->hasAccess()){
				$templateVars['header']    = $this->pageModel->title;
				$templateVars['pageTitle'] = $this->pageModel->title;
				$templateVars['content']   = $this->pageModel->content;
				$this->reg->Template->fillTemplate($templateVars);
			}
			else{
				$this->loginRequired('', false);
			}
		}
	}