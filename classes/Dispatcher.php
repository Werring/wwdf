<?php
	/**
	 * Controler_Dispatcher
	 *
	 * <p>Will dispatch the URL to the correct class with given parameters<br/>
	 * dispatch.ini will define what URLS are being dispatched to where</p>
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Core
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Dispatcher {
		/**
		 * Link to the Registry
		 *
		 * @var Registry
		 */
		private $reg;

		/**
		 * Contains dispatch.ini
		 *
		 * @var array
		 */
		private $ini;

		/**
		 * Contains all parameters for the page
		 *
		 * @var array
		 */
		private $pageVar = array();

		/**
		 * Current page
		 *
		 * @var string URL
		 */
		public $page;

		/**
		 * Link to the dispatcher log
		 *
		 * @var Model_Logfile
		 */
		private $log;

		/**
		 * Constructor
		 */
		public function __construct(){
			$this->log                       = Model_Logfile::getLog("Dispatcher", 5);
			$this->reg                       = Registry::getInstance();
			$this->ini                       = parse_ini_file("classes/dispatch.ini", true);
			$this->ini['default']['URL']     = '*';
			$this->ini['default']['param'][] = 'param';
		}

		/**
		 * Makes sure the given page will be send to the right handler
		 *
		 * @param $page
		 */
		public function handle($page){
			$this->page = strtolower($page);
			$class      = null;
			$name       = '';
			$value      = array();
			foreach($this->ini as $name => $value)
			{
				$match = $this->matchURL($value['URL'], $this->page);

				if(false !== $match){
					$class = "Controller_" . ucfirst($value['class']);
					if(isset($value['param']) && is_array($value['param'])){
						if(count($value['param']) <= count($match)){
							for($i = 0; $i < count($value['param']); $i++)
							{
								$this->pageVar[$value['param'][$i]] = $match[$i];
							}
						}
						else
						{
							$this->log->write('To many parameters given for ' . $name . '. Expected ' . count($match) . ', but given ' . count($value['param']));
							die('Error: To many parameters given for ' . $name . '. Expected ' . count($match) . ', but given ' . count($value['param']));
						}
					}
					break;
				}
			}
			$this->log->write('Directing to ' . $class . ' on rule: ' . $name . ' (' . $value['URL'] . ')');
			$this->log->write("Parameters:\n" . var_export($this->pageVar, true));
			$this->reg->Page = new $class($this->pageVar);
		}

		/**
		 * Match $url with $page
		 *
		 * Match a wildcardURL with the current page<br/>
		 * and gives the wildcards as array<br/>
		 * returns false on no match<br/>
		 *
		 * @param $url
		 * @param $page
		 *
		 * @return array|bool
		 */
		private function matchURL($url, $page){
			$eUrl    = explode("*", $url);
			$matches = false;
			if(fnmatch($url, $page)){
				$matches = explode("|", trim(str_replace($eUrl, "|", $page), "|/"));
				for($i = 0; $i < count($matches); $i++){
					if(false !== strpos($matches[$i], '/')){
						$matches[$i] = explode('/', $matches[$i]);
					}
				}
			}
			return $matches;
		}
	}
