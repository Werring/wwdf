<?php
	/**
	 * Base model
	 *
	 * Can not be called directly, has to be extended
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Model_Base {
		/**
		 * Link to Registry
		 *
		 * @access protected
		 * @var Registry
		 */
		protected $reg;

		/**
		 * Sets $reg to Registry
		 *
		 * @access protected
		 * @final
		 */
		protected final function  initModel(){
			$this->reg = Registry::getInstance();
		}

	}
