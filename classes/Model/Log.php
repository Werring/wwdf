<?php
	/**
	 * Load logfile and returning its content
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Model_Log {
		/**
		 * Link to Model_Log or string with log content
		 *
		 * @access protected
		 * @var Model_Log|string
		 */
		protected $data;

		/**
		 * Constructor loads given log
		 *
		 * Creates 404 if log not found
		 *
		 * @access public
		 *
		 * @param $name
		 */
		public function __construct($name){
			if($name == 'latest' && file_exists('logs')){
				$logs       = array_merge(array_diff(scandir('logs'), array('..', '.')));
				$pinfo      = pathinfo(array_pop($logs));
				$this->data = new Model_Log($pinfo['filename']);
			}
			else{
				$file = 'logs/' . $name . ".log";
				if(file_exists($file)){
					Model_Logfile::writeLog('Logviewer', "Display: " . $file);
					$this->data = file_get_contents($file);
				}
				else
				{
					Controller_Page_404::create404("/" . $name . '.log');
				}
			}
		}

		/**
		 * Returns data value of given log (Magic method __toString)
		 *
		 * @access public
		 * @return string
		 */
		public function __toString(){
			return strval($this->data);
		}

	}
