<?php
	/**
	 * Model_Mail
	 *
	 * <p>
	 * <code>$mail = new Model_Mail();
	 * $mail->from("jane.doe@test.com");
	 * $mail->to("john.doe@test.com");
	 * $mail->inlineAttachment("./templates/default/img/logo.png");
	 * $bodyText = <<<HTML
	 * <h1>some HTML</h1>
	 * <img src='cid:logo.png' alt='logo' />
	 *                              HTML;
	 * $mail->body($bodyText,$mail::HTML);
	 * $mail->send();</code>
	 * </p>
	 *
	 * @author                      werring <info@werringweb.nl>
	 * @package                     Model
	 * @subpackage                  Mail
	 * @copyright                   2009-2012 Werring webdevelopment
	 *
	 * @propery-read string $uid
	 * @propery-read string $subject
	 * @propery-read string $plainTextBody
	 * @propery-read string $htmlBody
	 * @propery-read string $from
	 * @propery-read string $to
	 * @propery-read string $cc
	 * @propery-read string $bcc
	 * @propery-read int    $priority
	 * @propery-read Array  $toArray
	 * @propery-read Array  $bccArray
	 * @propery-read Array  $ccArray
	 * @propery-read Array  $attachments
	 * @propery-read Array  $inlineElements
	 * @propery-read Array  $priorities
	 * @propery-read string $organization

	 */
	class Model_Mail extends Model_Base {

		###############
		# Mail limits #
		###############
		/**
		 * Minimal body size
		 */
		const minBodySize    = 1;
		/**
		 * Minimal subject size
		 */
		const minSubjectSize = 1;
		/**
		 * Maximal "to" addresses
		 */
		const maxTo          = 10;
		/**
		 * Minimal "to" addresses
		 */
		const minTo          = 1;
		/**
		 * Maximal "cc" addresses
		 */
		const maxCc          = 10;
		/**
		 * Maximal "bcc" addresses
		 */
		const maxBcc         = 10;
		/**
		 * Errors triggered by mailclass are this level
		 */
		const mailErrorLevel = 2;

		################
		# HTML / PLAIN #
		################
		const HTML  = true;
		const PLAIN = false;
		/**
		 * Unique id
		 *
		 * @var string
		 * @access protected
		 */
		protected $uid;
		/**
		 * Mail subject
		 *
		 * @var string
		 * @access protected
		 */
		protected $subject = "";
		/**
		 * Mail plain text body
		 *
		 * @var string
		 */
		protected $plainTextBody = "";
		/**
		 * Mail plain text body
		 *
		 * @var string
		 * @access protected
		 */
		protected $htmlBody = "";
		/**
		 * Mail from
		 *
		 * @var string
		 * @access protected
		 */
		protected $from = "no reply <noreply@werringweb.nl>";
		/**
		 * Mail to
		 *
		 * @var string
		 * @access protected
		 */
		protected $to = "John Doe <john.do@example.com>";
		/**
		 * Mail bcc
		 *
		 * @var string
		 * @access protected
		 */
		protected $bcc = "";
		/**
		 * Mail cc
		 *
		 * @var string
		 * @access protected
		 */
		protected $cc = "";
		/**
		 * Mail priority
		 *
		 * @var int 1≥$priority≥4
		 * @access protected
		 */
		protected $priority = 0;
		/**
		 * Mail to
		 *
		 * @var array
		 * @access protected
		 */
		protected $toArray = array();
		/**
		 * Mail bcc
		 *
		 * @var array
		 * @access protected
		 */
		protected $bccArray = array();
		/**
		 * Mail cc
		 *
		 * @var array
		 * @access protected
		 */
		protected $ccArray = array();
		/**
		 * Mail attachments
		 *
		 * @var array with MailAttachment
		 * @see    MailAttachment
		 * @access protected
		 */
		protected $attachments = array();
		/**
		 * Mail body inline images
		 *
		 * @var array (assoc) with MailAttachment
		 * @see    MailAttachment
		 * @access protected
		 */
		protected $inlineElements = array();
		/**
		 * Mail Priority list
		 *
		 * @var array
		 * @access protected
		 */
		protected $priorities = array(null, '1 (Highest)', '2 (High)', '3 (Normal)', '4 (Low)', '5 (Lowest)');
		/**
		 * Organization
		 *
		 * @var string
		 * @access protected
		 */
		protected $organization = "";

		/**
		 * Constructor
		 *
		 * @access public
		 *
		 * @param string $body     Email Content
		 * @param string $subject  Email Subject
		 * @param string $to       Mail Addresses (John Doe <john.doe@example.com>[,janedoe@example.com[,...]])
		 * @param string $from     Mail Address (John Doe <john.doe@example.com> | janedoe@example.com)
		 * @param string $cc       Mail Addresses (John Doe <john.doe@example.com>[,janedoe@example.com[,...]])
		 * @param string $bcc      Mail Addresses (John Doe <john.doe@example.com>[,janedoe@example.com[,...]])
		 * @param int    $priority Urgency Level (Highest 1, Lowest 4);
		 */
		public function __construct($body = "", $subject = "", $to = "", $from = '', $cc = "", $bcc = "", $priority = 4){
			$this->initModel();
			$this->uid = md5(uniqid(time()));
			$this->body($body);
			$this->subject($subject);
			$this->from($from);
			$this->to($to);
			$this->cc($cc);
			$this->bcc($bcc);
			$this->priority($priority);
		}

		/**
		 * Attach file to email
		 *
		 * @access public
		 *
		 * @param string $file
		 */
		public function attach($file){
			$this->attachments[] = new MailAttachment($file);
		}

		/**
		 * Attach file to use as inline file
		 *
		 * @access public
		 *
		 * @param string $file
		 */
		public function inlineAttachment($file){
			$this->inlineElements[] = new MailAttachment($file);
		}

		/**
		 * Getter for protected vars
		 *
		 * @access public
		 * @internal
		 *
		 * @param $name
		 *
		 * @return string
		 */
		public function __get($name){
			$return = '';
			switch($name){
				default:
					if(isset($this->$name)) $return = $this->$name;
					break;
			}
			return $return;
		}

		/**
		 * Set html/plaintext body to value
		 *
		 * @access public
		 *
		 * @param string $value
		 * @param bool   $html
		 */
		public function body($value, $html = false){
			if(!empty($value)){
				if($html) $this->htmlBody = strval($value);
				else   $this->plainTextBody = strval($value);
			}
		}

		/**
		 * Set subject to value and strip new lines
		 *
		 * @access public
		 *
		 * @param string $value
		 */
		public function subject($value){
			if(!empty($value)) $this->subject = str_replace(array("\n", "\r"), '', strval($value));
		}

		/**
		 * Set from to value and strip new lines
		 *
		 * @access public
		 *
		 * @param string $value
		 */
		public function from($value){
			if(!empty($value)) $this->from = str_replace(array("\n", "\r"), '', strval($value));
		}

		/**
		 * Set To to value and strip new lines
		 *
		 * @access public
		 *
		 * @param string $value Mail Addresses (John Doe <john.doe@example.com>[,janedoe@example.com[,...]])
		 */
		public function to($value){
			if(!empty($value)) $this->toArray[] = str_replace(array("\n", "\r"), '', strval($value));

		}

		/**
		 * Set cc to value and strip new lines
		 *
		 * @access public
		 *
		 * @param string $value Mail Addresses (John Doe <john.doe@example.com>[,janedoe@example.com[,...]])
		 */
		public function cc($value){
			if(!empty($value)) $this->ccArray[] = str_replace(array("\n", "\r"), '', strval($value));
		}

		/**
		 * set bcc to value and strip new lines
		 *
		 * @access public
		 *
		 * @param string $value Mail Addresses (John Doe <john.doe@example.com>[,janedoe@example.com[,...]])
		 */
		public function bcc($value){
			if(!empty($value)) $this->bccArray[] = str_replace(array("\n", "\r"), '', strval($value));
		}

		/**
		 * Set priority to value if 1≥$value≥4
		 *
		 * @access public
		 *
		 * @param int $value
		 */
		public function priority($value){
			$value = intval($value);
			if($value >= 1 && $value <= 5){
				$this->priority = $value;
			}
		}

		/**
		 * Set organization to value and strip new lines
		 *
		 * @access public
		 *
		 * @param $value
		 */
		public function organization($value){
			if(!empty($value)) $this->organization = str_replace(array("\n", "\r"), '', strval($value));
		}

		/**
		 * Send mail
		 *
		 * @access public
		 * @return bool
		 */
		public function send(){
			$this->to  = (count($this->toArray) > 0) ? implode(",", $this->toArray) : $this->to;
			$this->cc  = (count($this->ccArray) > 0) ? implode(",", $this->ccArray) : null;
			$this->bcc = (count($this->bccArray) > 0) ? implode(",", $this->bccArray) : null;
			if(!$this->checkMail()) return false;
			$header = $this->formatHeader();
			$body   = $this->formatBody();
			Model_Logfile::writeLog('mail', "to: " . $this->to . PHP_EOL . "subject: " . $this->subject . PHP_EOL . "Header: " . PHP_EOL . $header . PHP_EOL . "Body: " . $body, 8);
			return mail($this->to, $this->subject, $body, $header);
		}

		/**
		 * Check if all mail parameters are good to go
		 *
		 * @access protected
		 * @return bool
		 */
		protected function checkMail(){
			if(!empty($this->htmlBody) && empty($this->plainTextBody)) $this->plainTextBody = strip_tags($this->htmlBody);
			elseif(empty($this->htmlBody) && !empty($this->plainTextBody)) $this->htmlBody = nl2br($this->plainTextBody);
			$ok               = true;
			$subLen           = strlen($this->subject);
			$plainTextBodyLen = strlen($this->plainTextBody);
			$htmlBodyLen      = strlen($this->htmlBody);
			$toCount          = substr_count($this->to, ",") + 1;
			$bccCount         = substr_count($this->bcc, ",") + 1;
			$ccCount          = substr_count($this->cc, ",") + 1;
			/**
			 * Check Subject
			 */
			if(!is_string($this->subject) || $subLen < self::minSubjectSize){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'Subject is not a valid Subject. ' . $this->subject . ' (' . $subLen . '≥' . self::minSubjectSize . ')', self::mailErrorLevel);
			}
			/**
			 * Check Plain Text Body
			 */
			if(!is_string($this->plainTextBody) || $plainTextBodyLen < self::minBodySize){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'PlainTextBody is not a valid PlainTextBody. ' . $this->body . ' (' . $plainTextBodyLen . '≥' . self::minBodySize . ')', self::mailErrorLevel);
			}
			/**
			 * Check HTML Body
			 */
			if(!is_string($this->htmlBody) || $htmlBodyLen < self::minBodySize){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'HtmlBody is not a valid HtmlBody. ' . $this->body . ' (' . $htmlBodyLen . '≥' . self::minBodySize . ')', self::mailErrorLevel);
			}
			/**
			 * Check To
			 */
			if(!is_string($this->to) || $toCount > self::maxTo || $toCount < self::minTo || !self::validateMail($this->to, false, true)){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'To is not a valid To. ' . $this->to . ' (' . self::minTo . '<' . $toCount . '<' . self::maxTo . ')', self::mailErrorLevel);
			}
			/**
			 * Check CC
			 */
			if(!is_null($this->cc) && (!is_string($this->cc) || substr_count($this->cc, ",") > self::maxCc || !self::validateMail($this->cc, false, true))){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'Cc is not a valid Cc. ' . $this->cc . ' (' . $ccCount . '≤' . self::maxCc . ')', self::mailErrorLevel);
			}
			/**
			 * Check BCC
			 */
			if(!is_null($this->bcc) && (!is_string($this->bcc) || substr_count($this->bcc, ",") > self::maxBcc || !self::validateMail($this->bcc, false, true))){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'Bcc is not a valid Bcc. ' . $this->bcc . ' (' . $bccCount . '≤' . self::maxBcc . ')', self::mailErrorLevel);
			}
			/**
			 * Check Priority
			 */
			if(!is_numeric($this->priority) || $this->priority > 5 || $this->priority < 1){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'Priority is not a valid Priority. ' . $this->priority . ' (1≤' . $this->bcc . '≤4)', self::mailErrorLevel);
			}
			/**
			 * Check From
			 */
			if(!is_string($this->from) || !self::validateMail($this->from)){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'From is not a valid From. ' . $this->from, self::mailErrorLevel);
			}
			/**
			 * Check Organizations
			 */
			if(!is_string($this->organization) && !empty($this->organization)){
				$ok = false;
				Model_Logfile::writeLog('MailError', 'Organization is not a valid Organization. ' . $this->from, self::mailErrorLevel);
			}
			/**
			 * Check Attachments
			 */
			for($i = 0; $i < count($this->attachments); $i++){
				if(!$this->attachments[$i]->ok){
					$ok = false;
					break;
				}
			}
			/**
			 * Check Inline Attachments
			 */
			for($i = 0; $i < count($this->inlineElements); $i++){
				if(!$this->inlineElements[$i]->ok){
					$ok = false;
					break;
				}
			}
			return $ok;
		}

		/**
		 * Validate if we have a correct email.
		 *
		 * Allowed formats:
		 *
		 * * name <user@hostname.tld>
		 * * user@hostname.tld
		 *
		 * @access    public
		 * @static
		 *
		 * @param      $inputMail
		 * @param bool $strict only allow user[at]host.tld
		 * @param bool $list   input is a comma seperated list of mails
		 *
		 * @return bool
		 */
		public static function validateMail($inputMail, $strict = false, $list = false){
			if($list){
				$isOk         = true;
				$mailAddreses = explode(",", $inputMail);
				for($i = 0; $i < count($mailAddreses); $i++){
					if(self::validateMail($mailAddreses[$i], $strict)) continue;
					$isOk = false;
					break;
				}
			}
			else{
				$isOk = false;
				$mail = ($strict) ? $inputMail : preg_replace("/^[\w\s]*<([\w\@\.\_\-]*)>$/", "$1", $inputMail);
				if(preg_match("/^(\w+((-\w+)|(\w.\w+))*)\@(\w+((\.|-)\w+)*\.\w+)$/", $mail)){
					$isOk = true;
				}
				else Model_Logfile::writeLog('MailError', 'Given mailaddress is not a valid address. ' . $inputMail, self::mailErrorLevel);
			}
			return $isOk;
		}

		/**
		 * Format the mail header
		 *
		 * @access protected
		 * @return string Header
		 */
		protected function formatHeader(){
			$uid      = $this->uid;
			$priority = $this->priorities[$this->priority];

			$header = "From: " . $this->from . PHP_EOL;
			$header .= (empty($this->cc)) ? "" : "Cc: " . $this->cc . PHP_EOL;
			$header .= (empty($this->bcc)) ? "" : "Bcc: " . $this->bcc . PHP_EOL;
			$header .= "mailed-by: " . $_SERVER['SERVER_NAME'] . PHP_EOL;
			$header .= "X-Mailer: PHP/Werringwebdevelopment" . PHP_EOL;
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed;boundary=\"PHP-mixed-{$uid}\"" . PHP_EOL;
			$header .= (empty($priority)) ? "" : "Priority: " . $priority . PHP_EOL;
			$header .= (empty($this->organization)) ? "" : "Organization: " . $this->organization . PHP_EOL;

			return $header;
		}

		/**
		 * Format the mail body
		 *
		 * @access protected
		 * @return string Body
		 */
		protected function formatBody(){
			$uid = $this->uid;

			$body = <<<EOBODY
--PHP-mixed-{$uid}
Content-Type: multipart/related; boundary="PHP-rel-{$uid}"

--PHP-rel-{$uid}
Content-Type: multipart/alternative; boundary="PHP-alt-{$uid}"

--PHP-alt-{$uid}
Content-Type: text/plain

{$this->plainTextBody}

--PHP-alt-{$uid}
Content-Type: text/html

{$this->htmlBody}

EOBODY;
			$body .= <<<EOBODY
--PHP-alt-{$uid}--


EOBODY;
			for($i = 0; $i < count($this->inlineElements); $i++){
				$inline = $this->inlineElements[$i];
				if(!$inline->ok) continue;
				$body .= "--PHP-rel-{$uid}" . PHP_EOL;
				$body .= "Content-Type: " . $inline->mime . PHP_EOL;
				$body .= "Content-Disposition: inline\r\n";
				$body .= "Content-ID: " . $inline->name . PHP_EOL;
				$body .= "Content-Transfer-Encoding: base64" . PHP_EOL . PHP_EOL;
				$body .= $inline->content . PHP_EOL . PHP_EOL;
			}
			$body = trim($body) . PHP_EOL;
			$body .= "--PHP-rel-{$uid}--" . PHP_EOL . PHP_EOL;
			for($i = 0; $i < count($this->attachments); $i++){
				$attachment = $this->attachments[$i];
				if(!$attachment->ok) continue;
				$body .= "--PHP-mixed-{$uid}" . PHP_EOL;
				$body .= "Content-Type: " . $attachment->mime . "; name=\"" . $attachment->name . "\"" . PHP_EOL;
				$body .= "Content-Disposition: attachment; filename=\"" . $attachment->name . "\"" . PHP_EOL;
				$body .= "Content-Transfer-Encoding: base64" . PHP_EOL;
				$body .= $attachment->content . PHP_EOL . PHP_EOL;
			}
			$body = trim($body) . PHP_EOL;
			$body .= "--PHP-mixed-{$uid}--";
			return $body;
		}
	}

	/**
	 * MailAttachment
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Model
	 * @subpackage Mail
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class MailAttachment {
		/**
		 * Attachment is loaded
		 *
		 * @access public
		 * @var bool
		 */
		public $ok = false;

		/**
		 * Attachment name
		 *
		 * @access public
		 * @var string
		 */
		public $name = "";
		/**
		 * Attachment content (base64)
		 *
		 * @access public
		 * @var string
		 */
		public $content = "";
		/**
		 * Mimetype
		 *
		 * @access public
		 * @var string
		 */
		public $mime = "";

		/**
		 * Constructor
		 *
		 * @access public
		 *
		 * @param string|null $file
		 */
		public function __construct($file = null){
			if(is_null($file)) return;
			$this->addFile($file);
		}

		/**
		 * Add file
		 *
		 * @access public
		 *
		 * @param string $file
		 *
		 * @return bool
		 */
		public function addFile($file){
			if(!file_exists($file)){
				Model_Logfile::writeLog("MailError", 'File "' . $file . '" could not be found.', Model_Mail::mailErrorLevel);
				return;
			}
			if(!is_readable($file)){
				Model_Logfile::writeLog("MailError", 'File "' . $file . '" could not be read.', Model_Mail::mailErrorLevel);
				return;
			}
			$this->name    = basename($file);
			$this->content = chunk_split(base64_encode(file_get_contents($file)));
			$this->mime    = mime_content_type($file);
			$this->ok      = true;
		}
	}