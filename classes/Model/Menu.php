<?php
	/**
	 * Creating a menu
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-]2012 Werring webdevelopment
	 */
	class Model_Menu extends Model_Base
	{
		/**
		 * Menu array
		 *
		 * @var array
		 */
		protected $menu = array();

		/**
		 * Constructor
		 */
		public function __construct(){
			$this->initModel();
		}

		/**
		 * Add a menu item
		 *
		 * @param string $name   Name of menuitem as will be seen in menu
		 * @param string $uri    URI of destination
		 * @param string $target target attribute
		 * @param bool   $force  overwrite previous URL
		 */
		public function addMenuItem($name,$uri,$target='_self',$force=FALSE){
			$md5 = md5($name . $uri);
			if(!isset($this->menu[$md5]) || $force){
				$page = (substr($uri,0,1) == '/') ? substr($uri,1) : '/external';
				$this->menu[$md5] = array(
					"name"   => $name,
					"uri"    => $uri,
					"target" => $target,
					"page"   => $page
				);
			}
		}

		/**
		 * Remove a menuitem
		 *
		 * @param string $name Name of menuitem
		 * @param string $uri  URI of menuitem
		 */
		public function removeMenuitem($name,$uri){
			$md5 = md5($name . $uri);
			if(isset($this->menu[$md5])){
				unset($this->menu[$md5]);
			}
		}

		/**
		 * Add all pages from menu to the ACP
		 *
		 * @param string $prefix
		 */
		public function getPagesFromDB($prefix="/"){
			$result = $this->reg->DB->select('pages',"*");
			for($i=0;$i<$result['affected'];$i++){
				if($result[$i]['loginRequired'] == '0' || $this->reg->User->loggedIn){
					$this->addMenuItem($result[$i]['title'],$prefix . $result[$i]['path']);
				}
			}
		}

		/**
		 * Delete all menu items
		 */
		public function clearMenu(){
			$this->menu = array();
		}

		/**
		 * Render the menu
		 */
		public function renderMenu(){
			$reg = Registry::getInstance();
			$reg->Template->addBlock('menuitem', 'menu', $this->menu);
			$reg->Template->setVar('menu:selected:' . $reg->Dispatch->page, 'selected');
			$reg->Template->setWildVar('menu:selected:*', '');
		}

	}
