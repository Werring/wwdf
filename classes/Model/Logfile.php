<?php
	/**
	 * Model_Logfile
	 * <p>Log system for writing logs to files</p>
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */

	class Model_Logfile {
		#####################################
		# STATIC                            #
		#####################################
		#                                   #
		# (Scroll down for non-static part) #
		#                                   #
		#####################################

		/**
		 * All Model_Logfile's
		 *
		 * @access private
		 * @var array
		 */
		private static $logs = array();

		/**
		 * Get log object, or creates one for the given type
		 *
		 * @static
		 * @access public
		 *
		 * @param null $name
		 * @param int  $level
		 *
		 * @return Model_Logfile
		 */
		public static function getLog($name = null, $level = 10){
			$name = is_null($name) ? uniqid('log') : $name;
			if(!isset(self::$logs[$name])){
				self::$logs[$name] = new Model_Logfile($name, $level);
			}
			return self::$logs[$name];
		}

		/**
		 * Directly write to a Model_Log
		 *
		 * @static
		 *
		 *
		 * @param string $name   name of log to write to
		 * @param string $string value to append to log
		 * @param int    $level  level of log (default: 10)
		 *                       Write to given log without the need of a log object
		 */
		public static function writeLog($name, $string, $level = 10){
			$log = self::getLog($name, $level);
			if($log->level > $level) $log->level = $level;
			$log->write($string);
		}

		/**
		 * Write all logs to file and clears logs array
		 *
		 * @access public
		 * @static
		 */
		public static function finalizeLogs(){
			$file = __PATH__ . "logs/" . date("Ymd") . ".log";
			if(!file_exists($file)){
				if(!file_exists(dirname($file))){
					self::writeLog('Logfile', 'Creating log directory');
					mkdir(dirname($file));
				}
				self::writeLog('Logfile', 'Creating log file: ' . basename($file));
				touch($file);
			}

			$mailBody = "";
			$fileText = "";
			foreach(self::$logs as $name => $log)
			{
				$fileText .= self::toFile($log);
				$mailBody .= self::asMail($log);
			}

			if(strlen($mailBody) > 0){
				require_once __PATH__ . 'classes/Model/Mail.php';
				$m = new Model_Mail($mailBody, "Errors of level " . Registry::getInstance()->Conf->errorLevel['mail'] . ' or lower have been found:', Registry::getInstance()->Conf->admin);
				if(!$m->send()){
					$fileText .= self::toFile(self::getLog('mail'));
				}
			}
			file_put_contents($file, $fileText, FILE_APPEND);
			self::$logs = array();
		}

		/**
		 * Return logtext that has to be writen to file
		 *
		 * @static
		 *
		 * @access private
		 *
		 * @param Model_Logfile $log
		 *
		 * @return string
		 */
		private static function toFile(Model_Logfile $log){
			if($log->level > Registry::getInstance()->Conf->errorLevel['file']) return '';
			return self::toString($log);
		}

		/**
		 * If we have to mail this log, return it
		 *
		 * @static
		 *
		 * @access private
		 *
		 * @param Model_Logfile $log
		 *
		 * @return string
		 */
		private static function asMail(Model_Logfile $log){
			if($log->level > Registry::getInstance()->Conf->errorLevel['mail']) return '';
			return self::toString($log);
		}

		/**
		 * Stringify a log
		 *
		 * @access public
		 * @static
		 *
		 * @param $log
		 *
		 * @return string
		 */
		private static function toString($log){
			$prefix = date("H:i:s") . " " . strtoupper($log->name) . ":" . str_repeat(" ", 15 - strlen($log->name));
			$write  = "";
			for($j = 0; $j < count($log->log); $j++)
			{
				$message  = $log->log[$j];
				$messages = explode("\n", $message);
				for($i = 0; $i < count($messages); $i++)
				{
					if(count($messages) > 1 && count($messages) < 10){
						if($i == 0){
							$write .= $prefix . $i . ". " . $messages[$i];
						}
						else
						{
							$write .= str_repeat(" ", strlen($prefix)) . $i . ". " . $messages[$i];
						}
					}
					elseif(count($messages) > 1)
					{
						if($i == 0){
							$write .= $prefix . " " . $i . ". " . $messages[$i];
						}
						elseif($i < 10)
						{
							$write .= str_repeat(" ", strlen($prefix)) . " " . $i . ". " . $messages[$i];
						}
						else
						{
							$write .= str_repeat(" ", strlen($prefix)) . $i . ". " . $messages[$i];
						}

					}
					else
					{
						$write .= $prefix . $messages[$i];
					}
					$write .= PHP_EOL;
				}
			}
			return $write;
		}

		##############
		# non-static #
		##############
		/**
		 * Log entries (String)
		 *
		 * @var array
		 * @access public
		 */
		private $log = array();

		/**
		 * Log name
		 *
		 * @var string
		 * @access public
		 */
		public $name = null;

		/**
		 * Log level
		 *
		 * @var int
		 * @access public
		 */
		public $level;

		/**
		 * Private Constructor use getLog instead
		 *
		 * @access private
		 * @final
		 *
		 * @param string $name
		 * @param int    $level
		 *
		 * @throw  Exception
		 *         Will throw Exception when level < 1
		 */
		private final function __construct($name, $level){
			$level = ($level);
			if($level <= 0) throw(new Exception('Level can\'t be lower as 1'));
			$this->name  = $name;
			$this->level = $level;
			if(!isset(self::$logs[$name])){
				self::$logs[$name] = array();
			}
		}

		/**
		 * Write data to log
		 *
		 * @access public
		 *
		 * @param array|string $data
		 */
		public function write($data){
			$string      = (is_array($data)) ? implode("\n", $data) : $data;
			$this->log[] = trim($string);
		}

		/**
		 * Getter for the $log variable
		 *
		 * @access public
		 * @return array
		 */
		public function  readLog(){
			return $this->log;
		}

	}
