<?php
	/**
	 * Loads config.php
	 *
	 * Loads config.php, and make them readable
	 *
	 * @author                              werring <info@werringweb.nl>
	 * @package                             Model
	 * @copyright                           2009-2012 Werring webdevelopment
	 *
	 * @property-read String $template      Template name
	 * @property-read String $sitename      Name of site
	 * @property-read Array  $database      Database login
	 * @property-read String $md5Integrity  MD5 hash of /index.php
	 * @property-read String $admin         Admin email
	 * @property-read Array  $errorLevel    Mail and File log error level
	 */
	final class Model_Configuration {
		/**
		 * The data from config.php
		 *
		 * @var array $confuguration
		 * @access private
		 */
		private $configuration = array();

		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			require_once __PATH__ . 'config.php';
			$this->configuration = isset($__) ? $__ : array();
		}

		/**
		 * Magic getter for configuration
		 *
		 * @internal
		 * @access public
		 *
		 * @param $name
		 *
		 * @return null|mixed
		 */
		public function __get($name){
			return isset($this->configuration[$name]) ? $this->configuration[$name] : null;
		}
	}
