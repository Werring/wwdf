<?php
	/**
	 * Model_Database
	 *
	 * <p>Database connection class</p>
	 *
	 * @author    Thom Werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */

	class Model_Database extends Model_Base {
		/**
		 * Connect status
		 *
		 * @access public
		 * @var bool
		 */
		public $connected = false;

		/**
		 * Link to mysqli
		 *
		 * @access private
		 * @var mysqli
		 */
		private $sql = null;

		/**
		 * Last result
		 *
		 * @access private
		 * @var mysqli_result;
		 */
		private $result = null;
		/**
		 * Stores last query done
		 *
		 * @access private
		 * @var string
		 */
		private $lastQuery = null;

		/**
		 * Link to log class
		 *
		 * @access private
		 * @var Model_Logfile
		 */
		private $log;

		/**
		 * Constructor
		 */
		function __construct(){
			$this->initModel();
			$this->log = Model_Logfile::getLog('database', 8);
		}

		/**
		 * Connect to database
		 *
		 * @access public
		 *         connect to database
		 */
		public function connect(){
			if(is_null($this->sql)){
				$db_host         = $this->reg->Conf->database['host'];
				$db_user         = $this->reg->Conf->database['user'];
				$db_pass         = $this->reg->Conf->database['pass'];
				$db_daba         = $this->reg->Conf->database['daba'];
				$this->sql       = @new mysqli($db_host, $db_user, $db_pass, $db_daba);
				$this->connected = true;
				if(mysqli_connect_error()){
					$this->log->write(mysqli_connect_error());
					die('can\'t connect to database!');
				}
			}
		}

		/**
		 * Creates a select query and runs it.
		 *
		 * @access public
		 *
		 * @param String       $table   table name
		 * @param string|array $select  what fields needs to be selected
		 * @param String       $advance (Optional) advance mysql select query
		 *
		 * @return Array all rows within the query, and an assoc entry with affected rows or array with errors
		 */
		public function select($table, $select, $advance = ""){
			$table  = $this->prefixTable($table);
			$select = (is_array($select)) ? implode(",", $select) : $select;
			$qry    = "SELECT " . $select . " FROM " . $table . " " . $advance . ";";
			return $this->query($qry);
		}


		/**
		 * Inserts row into table
		 *
		 * @access public
		 *
		 * @param       String String table name
		 * @param Array $data  assoc array with data to be inserted
		 *
		 * @return Boolean succes
		 */
		public function insert($table, $data){
			if(!is_array($data) || !is_string($table)){
				return false;
			}
			$table  = $this->prefixTable($table);
			$query  = "INSERT INTO " . $table . " (";
			$fields = "";
			$values = "";
			foreach($data as $field => $value)
			{
				$fields .= $field . ", ";
				if(substr($value, -2, 2) == "()"){
					$values .= $value . ", ";
				}
				else
				{
					$values .= "'" . $value . "', ";
				}

			}
			$fields = substr($fields, 0, -2);
			$values = substr($values, 0, -2);
			$query .= $fields . ") " . PHP_EOL .
				"VALUES (" . $values . ")";
			$result = $this->query($query);
			return $result['succes'];
		}


		/**
		 * Deletes data from table
		 *
		 * @access public
		 *
		 * @param String $table Table name
		 * @param Array  $where assoc array with where clause
		 * @param String $type  how the where clause should be separated (AND / OR)
		 *
		 * @return Array an array containing affected rows or sql Error
		 */
		public function delete($table, $where, $type = "AND"){
			$table    = $this->prefixTable($table);
			$query    = "DELETE FROM " . $table . " WHERE ";
			$whereqry = "";
			foreach($where as $field=> $value)
			{
				$whereqry .= $field . "= '" . $value . "' " . $type . " ";
			}
			$whereqry = substr($whereqry, 0, -4);

			$query .= $whereqry;

			return $this->query($query);
		}

		/**
		 * Updates data in table
		 *
		 * @access public
		 *
		 * @param String $table   Table name
		 * @param Array  $data    assoc array with data to be updated
		 * @param Array  $where   assoc array with where clause
		 * @param String $type    how the where clause should be separated (AND / OR)
		 * @param String $advance (Optional) advance mysql update query
		 *
		 * @return Array an array containing affected rows or sql Error
		 */
		public function update($table, $data, $where, $type = "and", $advance = ""){
			$table = $this->prefixTable($table);
			$query = "UPDATE " . $table . " SET ";
			$set   = "";
			foreach($data as $field => $value)
			{
				if(is_numeric($value)) $set .= $field . "=" . $value . ",";
				else $set .= $field . "='" . $value . "',";
			}
			$set = substr($set, 0, -1);
			$query .= $set;
			$whereqry = " WHERE ";
			foreach($where as $field=> $value)
			{
				$whereqry .= $field . "= '" . $value . "' " . $type . " ";
			}
			$whereqry = substr($whereqry, 0, -4);

			$query .= $whereqry;
			$query .= " " . $advance;
			return $this->query($query);
		}

		/**
		 * Executes complete SQL query and tries to return data as good as possible
		 *
		 * REMEMBER TO PREFIX YOUR TABLE NAME!
		 *
		 * @access public
		 *
		 * @param String  $qry          a mysqlQry
		 * @param Boolean $returnResult should the $result variable be returned in the return array (Default: false)
		 *
		 * @return Array array with return data
		 */
		public function query($qry, $returnResult = false){

			$this->lastQuery = $qry;
			/**
			 * @var mysqli_result;
			 */
			$this->result = @$this->sql->query($qry);
			$return       = "";
			if($returnResult){
				$return["result"] = $this->result;
			}
			switch($this->result)
			{
				case false:
					$return["error"]  = $this->sql->error;
					$return["errno"]  = $this->sql->errno;
					$return['succes'] = false;
					break;
				case true:
					$return['affected'] = $this->sql->affected_rows;
					$return['succes']   = true;
					if(method_exists($this->result, "fetch_assoc")){
						while($row = $this->result->fetch_assoc())
						{
							$return[] = $row;
						}
						$return['affected'] = $this->result->num_rows;
						$this->result->close();
					}
					break;
			}
			$this->log->write($qry);
			$this->log->write("Result: " . PHP_EOL . var_export($return, true));
			return $return;
		}

		/**
		 * Executes complete SQL query and tries to return data as good as possible
		 *
		 * <p>REMEMBER TO PREFIX YOUR TABLE NAME!<br/>
		 * qry (Alias for Query)
		 * </p>
		 *
		 * @access public
		 *
		 * @param  String  $qry          a mysqlQry
		 * @param  Boolean $returnResult should the $result variable be returned in the return array (Default: false)
		 *
		 * @return Array array with return data

		 */
		public function qry($qry, $returnResult = false){
			return $this->query($qry, $returnResult);
		}

		/**
		 * Creates table in database
		 *
		 * <p>
		 * Format of $fields:
		 * <code>
		 * array(
		 *      fieldname1"=>array(
		 *          "type"=>"fieldtype",
		 *          "primary"=>true/false,
		 *          "advance"=>"NOT NULL AUTO_INCREMENT etc"
		 *      ),
		 *      "fieldname2"=>array(
		 *          ...
		 *      )
		 * )
		 * </code>
		 * </p>
		 *
		 * @access public
		 *
		 * @param String  $table   Table Name
		 * @param Array   $fields  Field data
		 * @param Boolean $force   Force creation of table <b>THIS CAN PERMANENTLY DESTROY DATA</b>
		 * @param String  $advance advance SQL Create query (optional)
		 *
		 * @return Boolean result
		 *
		 */
		public function createTable($table, $fields, $force = false, $advance = "ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci"){
			$table = $this->prefixTable($table);
			if($force){
				$succes = $this->query("DROP TABLE IF EXISTS " . $table);
				if(!$succes) $this->log->write($this->lastError());
			}
			$query = "CREATE TABLE IF NOT EXISTS " . $table . " (" . PHP_EOL;
			foreach($fields as $field=> $description)
			{
				$type = "";
				if(!isset($description["type"]) && is_array($description)){
					trigger_error("No type defined for " . $field, E_USER_ERROR);
				}
				elseif(!is_array($description))
				{
					$type = $description;
					unset($description);
				}
				else
				{
					$type = $description['type'];
				}
				$query .= $field . " " . $type . " ";
				;
				if(isset($description['primary']) && $description['primary'] === true){
					$query .= "PRIMARY KEY ";
				}
				if(isset($description['advance'])){
					$query .= $description['advance'];
				}
				$query .= " ," . PHP_EOL;
			}
			$query = substr($query, 0, -3);
			$query .= PHP_EOL . ") " . $advance;
			$result = $this->query($query, true);
			return $result['succes'];
		}

		/**
		 * Truncates an entire table
		 *
		 * @access public
		 *
		 * @param String $table Table name
		 *
		 * @return Boolean succes
		 */
		public function clearTable($table){
			$table = $this->prefixTable($table);
			return $this->sql->query("Truncate table " . $table);
		}

		/**
		 * Real Escape String escapes user submitted data before executing it by with mysql.
		 *
		 * @access public
		 *
		 * @param String $string Text needed to be escaped
		 *
		 * @return String escaped string, safe to write to database
		 */
		public function res($string){
			return $this->sql->real_escape_string($string);
		}

		/**
		 * Prefixes string with the table prefix
		 *
		 * @access public
		 *
		 * @param string $table Table name without prefix
		 *
		 * @return string table name with prefix without capitals
		 */
		public function prefixTable($table){
			return strtolower($this->reg->Conf->database['prefix'] . $table);
		}

		/**
		 * Returns error number, message the query that triggerd the error
		 *
		 * @access public
		 * @return String Error number, message and query
		 */
		public function lastError(){
			return $this->sql->error . " (" . $this->sql->errno . ")" . PHP_EOL . "The complete query was \"" . $this->lastQuery . "\"" . PHP_EOL . PHP_EOL;
		}

		/**
		 * Get last AUTO_INCREMENT value
		 *
		 * @access public
		 * @return int|string
		 */
		public function insertId(){
			return $this->sql->insert_id;
		}

	}
