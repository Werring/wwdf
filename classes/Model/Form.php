<?php
	/**
	 * Form creator
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Model_Form extends Model_Base {
		/**
		 * DOM of the form
		 *
		 * @access public
		 * @var DOMDocument
		 */
		protected $form;

		/**
		 * Element tree
		 *
		 * @access public
		 * @var array with DOMElement
		 */
		protected $elementTree = array();

		/**
		 * Constructor
		 *
		 * Creates basic form
		 *
		 * @access public
		 *
		 * @param null          $name               Form Name
		 * @param string        $action             Form Action
		 * @param array|string  $extraAttributes    Form Attributes
		 */
		public function __construct($name = null, $action = 'SELF', $extraAttributes = array()){
			$this->initModel();
			if(is_null($name)) $name = uniqid('form');
			if($action == 'SELF') $action = '/' . $this->reg->Dispatch->page;
			if(is_array($extraAttributes)){
				if(!isset($extraAttributes['method'])) $extraAttributes['method'] = 'POST';
			}
			elseif(is_string($extraAttributes)){
				$extraAttributes = array('method' => $extraAttributes);
			}
			else{
				$extraAttributes = array('method' => 'POST');
			}
			$this->form               = new DOMDocument();
			$this->form->formatOutput = true;
			$form                     = $this->form->createElement('form');
			$form->setAttribute('name', $name);
			$form->setAttribute('action', $action);
			$this->loopExtraAtributes($form, $extraAttributes);
			$this->form->appendChild($form);
			$this->elementTree[] = $form;
		}

		/**
		 * Start a fieldset with legend
		 *
		 * @access public
		 *
		 * @param string      $name
		 * @param null|string $legend
		 * @param array       $extraAttributes
		 */
		public function startFieldSet($name, $legend = null, $extraAttributes = array()){
			$this->dlEnd();
			if(is_null($legend)){
				$legend = $name;
			}
			$fieldSet = $this->form->createElement('fieldset');
			$fieldSet->setAttribute('id', 'Fieldset_' . $name);
			$legendElement = $this->form->createElement('legend', $legend);
			$fieldSet->appendChild($legendElement);
			$this->loopExtraAtributes($fieldSet, $extraAttributes);
			$this->elementTree[] = $fieldSet;
		}

		/**
		 * Close fieldset
		 *
		 * @access public
		 */
		public function closeFieldSet(){
			$this->dlEnd();
			$elem = $this->currentElement();
			if($elem instanceof DOMElement && $elem->tagName == 'fieldset'){
				$fieldSet = array_pop($this->elementTree);
				$elem     = $this->currentElement();
				$elem->appendChild($fieldSet);
			}
			else Model_Logfile::writeLog('Form', 'no fieldset to close');
		}

		/**
		 * Add inputfield to form
		 *
		 * @access public
		 *
		 * @param string        $name
		 * @param bool          $multiline
		 * @param null|string   $value
		 * @param array         $extraAtributes
		 */
		public function addInputField($name, $multiline = false, $value = null, $extraAtributes = array()){
			if($this->currentElement()->tagName != 'dl'){
				$this->elementTree[] = $this->form->createElement('dl');
			}
			$parent = $this->currentElement();
			$dt     = $this->form->createElement('dt');
			$dd     = $this->form->createElement('dd');
			$text   = $this->form->createTextNode($name);
			$dt->appendChild($text);
			if($multiline){
				$node = $this->form->createElement('textarea', $value);
			}
			else{
				$node = $this->form->createElement('input');
				$node->setAttribute('type', 'text');
				$node->setAttribute('value', $value);
			}

			$node->setAttribute('name', $name);
			$this->loopExtraAtributes($node, $extraAtributes);
			$dd->appendChild($node);
			$parent->appendChild($dt);
			$parent->appendChild($dd);

		}

		/**
		 * Add button to form
		 *
		 * @access public
		 *
		 * @param             $name
		 * @param string|null $type (reset|submit|button)
		 * @param string      $value
		 * @param array       $extraAtributes
		 */
		public function addButton($name, $type = null, $value = '', $extraAtributes = array()){
			$this->dlEnd();
			$parent = $this->currentElement();
			$button = $this->form->createElement('input');
			switch(strtolower($type)){
				case 'reset':
					$buttonType = 'reset';
					break;
				case 'submit':
					$buttonType = 'submit';
					break;
				default:
					$buttonType = 'button';
					break;
			}
			$button->setAttribute('name', $name);
			$button->setAttribute('type', $buttonType);
			$button->setAttribute('value', $value);
			$this->loopExtraAtributes($button, $extraAtributes);
			$parent->appendChild($button);
			$parent->appendChild($this->form->createElement('br'));
		}

		/**
		 * Add checkbox|radiobutton to form
		 *
		 * @access public
		 *
		 * @param string $displayName
		 * @param string $name
		 * @param array  $data with []['text'] and []['value']
		 * @param string $type
		 * @param int    $boxesPerLine
		 * @param bool   $useDL
		 * @param array  $extraAtributes
		 */
		public function addBox($displayName, $name, $data, $type = 'checkbox', $boxesPerLine = 1, $useDL = true, $extraAtributes = array()){
			if(!is_array($data)){
				Model_Logfile::writeLog('Form', 'invalid addBox, $data is not a string', 3);
				return;
			}
			if($boxesPerLine < 1) $boxesPerLine = 1;
			if($this->currentElement()->tagName != 'dl' && $useDL){
				$this->elementTree[] = $this->form->createElement('dl');
			}
			elseif($this->currentElement()->tagName == 'dl' && !$useDL){
				$this->dlEnd();
			}
			else{
				var_dump($this->currentElement()->tagName == 'dl', !$useDL);
			}
			$boxType = (strtolower($type) == 'checkbox') ? 'checkbox' : 'radio';
			$br      = $this->form->createElement('br');
			$text    = $this->form->createTextNode($displayName);
			$parent  = $this->currentElement();
			if($useDL){
				$dt        = $this->form->createElement('dt');
				$boxHolder = $this->form->createElement('dd');
				$dt->appendChild($text);
				$parent->appendChild($dt);
				$parent->appendChild($boxHolder);
			}
			else{
				$boxHolder = $parent;
			}
			for($i = 0; $i < count($data); $i++){
				if((($i) % ($boxesPerLine + 1)) == 1){
					$boxHolder->appendChild($br);
					var_dump($i, $boxesPerLine + 1, $i % ($boxesPerLine + 1));
				}
				$label = $this->form->createElement('label');
				$node  = $this->form->createElement('input');
				$node->setAttribute('value', $data[$i]['value']);
				$node->setAttribute('name', $name);
				$node->setAttribute('type', $boxType);
				$this->loopExtraAtributes($node, $extraAtributes);
				$textnode = ($data[$i]['text'] != '') ? $this->form->createTextNode($data[$i]['text']) : $text;
				$label->appendChild($node);
				$label->appendChild($textnode);
				$boxHolder->appendChild($label);
			}
			if(!$useDL){
				$boxHolder->appendChild($br);
			}

		}

		/**
		 * Output form to HTML
		 *
		 * Magic method __toString()
		 *
		 * @access public
		 * @return string
		 */
		public function __toString(){
			$this->dlEnd();
			for($i = count($this->elementTree) - 1; $i >= 0; $i--){
				$elemChild = $this->elementTree($i);
				if($elemChild->tagName == 'form'){
					$this->form->appendChild($elemChild);
				}
				else{
					$elemParent = $this->elementTree($i - 1);
					$elemParent->appendChild($elemChild);
				}
			}
			return $this->form->saveHTML();
		}

		/**
		 * Loop through all $attributes and set them to the given $element
		 *
		 * @access protected
		 *
		 * @param DOMElement $element
		 * @param array      $attributes
		 */
		protected function loopExtraAtributes(&$element, $attributes){
			if(is_array($attributes)){
				foreach($attributes as $key=> $value){
					$element->setAttribute($key, $value);
				}
			}
			else{
				var_dump($attributes);
			}
		}

		/**
		 * Returns last element from $elementTree
		 *
		 * @access protected
		 * @return DOMElement
		 */
		protected function currentElement(){
			/**
			 * @var DOMElement $elem
			 */
			$elem = $this->elementTree[count($this->elementTree) - 1];
			return $elem;
		}

		/**
		 * Return item from elementTree
		 *
		 * @access protected
		 *
		 * @param $i
		 *
		 * @return DOMElement
		 */
		protected function elementTree($i){
			return $this->elementTree[$i];
		}

		/**
		 * Close dl if it is the last element of the elementTree
		 *
		 * @access protected
		 */
		protected function dlEnd(){
			if($this->currentElement()->tagName == 'dl'){
				$dl = array_pop($this->elementTree);
				$this->currentElement()->appendChild($dl);
				$this->dlEnd();
			}
		}
	}
