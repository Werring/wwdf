<?php
	/**
	 * Will handle all headers
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Model_Header extends Model_Base {

		/**
		 * List of all known status codes
		 *
		 * @var array
		 */
		protected static $statusCode = array(
			"1.0"=>array(
				100=>"100 Continue",
				101=>"101 Switching Protocols",
				102=>"102 Processing",
				200=>"200 OK",
				201=>"201 Created",
				202=>"202 Accepted",
				204=>"204 No Content",
				205=>"205 Reset Content",
				206=>"206 Partial Content",
				207=>"207 Multi-Status",
				208=>"208 Already Reported",
				226=>"226 IM Used",
				300=>"300 Multiple Choices",
				301=>"301 Moved Permanently",
				302=>"302 Found",
				304=>"304 Not Modified",
				306=>"306 Switch Proxy",
				308=>"308 Permanent Redirect",
				400=>"400 Bad Request",
				401=>"401 Unauthorized",
				402=>"402 Payment Required",
				403=>"403 Forbidden",
				404=>"404 Not Found",
				405=>"405 Method Not Allowed",
				406=>"406 Not Acceptable",
				407=>"407 Proxy Authentication Required",
				408=>"408 Request Timeout",
				409=>"409 Conflict",
				410=>"410 Gone",
				410=>"411 Length Required",
				410=>"412 Precondition Failed",
				410=>"413 Request Entity Too Large",
				410=>"414 Request-URI Too Long",
				410=>"415 Unsupported Media Type",
				410=>"416 Requested Range Not Satisfiable",
				410=>"417 Expectation Failed",
				410=>"418 I'm a teapot",
				420=>"420 Enhance Your Calm",
				422=>"422 Unprocessable Entity",
				423=>"423 Locked",
				424=>"424 Failed Dependency",
				425=>"425 Unordered Collection",
				426=>"426 Upgrade Required",
				428=>"428 Precondition Required",
				429=>"429 Too Many Requests",
				431=>"431 Request Header Fields Too Large",
				444=>"444 No Response",
				449=>"449 Retry With",
				450=>"450 Blocked by Windows Parental Controls",
				499=>"499 Client Closed Request",
				500=>"500 Internal Server Error",
				501=>"501 Not Implemented",
				502=>"502 Bad Gateway",
				503=>"503 Service Unavailable",
				504=>"504 Gateway Timeout",
				505=>"505 HTTP Version Not Supported",
				506=>"506 Variant Also Negotiates",
				507=>"507 Insufficient Storage",
				508=>"508 Loop Detected",
				509=>"509 Bandwidth Limit Exceeded",
				510=>"510 Not Extended",
				511=>"511 Network Authentication Required",
				598=>"598 Network read timeout error",
				599=>"599 Network connect timeout error"
			),
			"1.1"=>array(
				203=>"203 Non-Authoritative Information",
				303=>"303 See Other",
				305=>"305 Use Proxy",
				307=>"307 Temporary Redirect"
			)
		);

		/**
		 * request headers
		 *
		 * @var array
		 */
		protected $requestHeaders;

		/**
		 * Constructor
		 */
		public function __construct(){
			$this->requestHeaders = (function_exists('getallheaders')) ? getallheaders() : array();
		}

		/**
		 * Set header
		 *
		 * @static
		 * @param string $type
		 * @param string $value
		 * @param bool $replace
		 */
		public function set($type,$value=null,$replace=false){
			if(!headers_sent()){
				if(is_null($value)) header($type,$replace);
				else header($type . ": " . $value,$replace);
			}
		}
		/**
		 * Set HTTP resonse header
		 *
		 * @static
		 * @param int $type
		 */
		public function setHttpStatusResponse($type){
			if(isset(self::$statusCode['1.0'][$type]))	{
				$header = "HTTP/1.0 " . self::$statusCode['1.0'][$type];
			} elseif(isset(self::$statusCode['1.1'][$type])) {
				$header = "HTTP/1.1 " . self::$statusCode['1.1'][$type];
			} else {
				$header = "HTTP/1.0 200 OK";
				Model_Logfile::writeLog('HEADER','No status response was found for ' . $type . '. Using 200 OK as default',2);
			}
			Model_Logfile::writeLog('HEADER',"Sending " . $header,8);
			$this->set($header);
		}
		/**
		 * Get the requestheader
		 *
		 * @param string $name
		 * @return string|bool
		 */
		public function requestHeader($name){
			return (isset($this->requestHeaders[$name])) ?$this->requestHeaders[$name] : false;
		}


	}
