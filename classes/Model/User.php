<?php
	/**
	 * Store user data
	 *
	 * Handles login, session and user storage
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 *
	 * @property-read string $id User ID
	 * @property-read string $name User name
	 * @property-read string email User email
	 */
	class Model_User extends Model_Base {

		/**
		 * User ID
		 *
		 * @access private
		 * @var int
		 */
		private $uid;
		/**
		 * User Name
		 *
		 * @access private
		 * @var string
		 */
		private $name;
		/**
		 * User Email
		 *
		 * @access private
		 * @var string
		 */
		private $email;
		/**
		 * User password sha1 hash
		 *
		 * @access private
		 * @var string
		 */
		private $passhash;
		/**
		 * Session sha1 hash
		 *
		 * @access private
		 * @var string
		 */
		private $sessionhash;
		/**
		 * Login expires after login time
		 *
		 * @access private
		 * @var int
		 */
		private $loginexpires = 3600;
		/**
		 * Is user logged in
		 *
		 * @access public
		 * @var bool
		 */
		public $loggedIn = false;

		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			$this->initModel();
		}

		/**
		 * Initialisation,
		 *
		 * Checks if we can log in from cookie data
		 *
		 * @access public
		 */
		public function init(){
			$this->loggedIn = $this->loginFromCookie();
		}

		/**
		 * Attempts to log in user with POST data
		 *
		 * @access public
		 */
		public function logIn(){
			if(!$this->loggedIn && $this->reg->Request->type('post'))
				$this->loggedIn = $this->loginFromPost();
			if($this->loggedIn){
				$this->reg->Headers->set('Location', $this->reg->Dispatch->page);
				die;
			}
		}

		/**
		 * Logs out user
		 *
		 * @access public
		 */
		public function logOut(){
			if($this->loggedIn) $this->unsetCookie();
		}

		/**
		 * Magic getter for id, name and email
		 *
		 * @internal
		 * @access public
		 *
		 * @param $string
		 *
		 * @return int|string
		 */
		public function __get($string){
			$return = '';
			switch($string)
			{
				case 'id':
					$return = $this->uid;
					break;
				case 'name':
					$return = $this->name;
					break;
				case 'email':
					$return = $this->email;
					break;
			}
			return $return;
		}

		/**
		 * Generate a session hash
		 *
		 * @access protected
		 *
		 * @param null|int    $time
		 * @param null|int    $uid
		 * @param null|string $uname
		 * @param null|string $uemail
		 * @param null|string $upasshash
		 *
		 * @return     string
		 */
		protected final function generateSessionHash($time = null, $uid = null, $uname = null, $uemail = null, $upasshash = null){
			$id        = (is_null($uid)) ? $this->uid : $uid;
			$name      = (is_null($uname)) ? $this->name : $uname;
			$email     = (is_null($uemail)) ? $this->email : $uemail;
			$passhash  = (is_null($upasshash)) ? $this->passhash : $upasshash;
			$timestamp = (is_null($time)) ? time() : $time;
			return sha1("[" . $timestamp . ':' . $id . ':' . $name . ':' . $email . ':' . $passhash . "]");
		}

		/**
		 * Set cookie and inserts session data in database
		 *
		 * @access protected
		 */
		protected function setCookie(){
			$data              = array();
			$data['hash']      = $this->sessionhash;
			$data['timestamp'] = time();
			$data['uid']       = $this->uid;
			$data['expire']    = $this->loginexpires;
			$cookievalue       = serialize($data);
			setcookie('WWDF_loginsession', $cookievalue, $data['expire'] + time(), '/', $_SERVER['HTTP_HOST']);
			$insert['uid'] = $this->uid;
			$this->reg->DB->delete('user_sessions', $insert);
			$insert['sessionhash'] = $this->sessionhash;
			$this->reg->DB->insert('user_sessions', $insert);

		}

		/**
		 * Deletes cookie and removes session from table
		 *
		 * @access protected
		 */
		protected function unsetCookie(){
			setcookie('WWDF_loginsession', '', time() - 3600, '/', $_SERVER['HTTP_HOST']);
			$this->reg->DB->delete('user_sessions', array('uid'=> $this->uid));
		}

		/**
		 * Log in with given username and password
		 *
		 * If keepalive is true, session stays alive for 1 year
		 *
		 * @access protected
		 * @return bool
		 */
		protected function loginFromPost(){
			$username    = $this->reg->Request->get('username')->asString();
			$password    = $this->reg->Request->get('password')->asString();
			$keepSession = $this->reg->Request->get('SaveSession')->asBool();
			$table       = 'users';
			$select      = '*';
			$username;
			$passhash = sha1($password);
			$ret      = $this->reg->DB->select($table, $select, 'WHERE `name`="' . $username . '" AND `passhash`="' . $passhash . '"');
			if($ret['affected'] == 0) return false;
			$this->uid         = $ret[0]['id'];
			$this->name        = $ret[0]['name'];
			$this->email       = $ret[0]['email'];
			$this->passhash    = $ret[0]['passhash'];
			$this->sessionhash = $this->generateSessionHash();
			if($keepSession) $this->loginexpires *= 24 * 365;
			$this->setCookie();

			return true;
		}

		/**
		 * Log in from cookie
		 *
		 * @access protected
		 * @return bool
		 */
		protected function loginFromCookie(){
			if(isset($_COOKIE['WWDF_loginsession'])){
				$data   = unserialize($_COOKIE['WWDF_loginsession']);
				$table  = 'user_sessions';
				$select = '*';
				$ret    = $this->reg->DB->select($table, $select, 'WHERE `uid`="' . $data['uid'] . '" AND `sessionhash`="' . $data['hash'] . '"');
				if($ret['affected'] == 1){
					$table  = 'users';
					$select = '*';
					$ret    = $this->reg->DB->select($table, $select, 'WHERE `id`="' . $data['uid'] . '"');
					if($data['hash'] == $this->generateSessionHash($data['timestamp'], $ret[0]['id'], $ret[0]['name'], $ret[0]['email'], $ret[0]['passhash'])){
						$this->uid          = $ret[0]['id'];
						$this->name         = $ret[0]['name'];
						$this->email        = $ret[0]['email'];
						$this->passhash     = $ret[0]['passhash'];
						$this->sessionhash  = $this->generateSessionHash();
						$this->loginexpires = $data['expire'];
						$this->setCookie();
						return true;
					}
				}
			}
			return false;
		}
	}
