<?php
	/**
	 * Quote confucius
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Model
	 * @subpackage Fun
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class Model_Confucius {
		/**
		 * All quotes
		 *
		 * @var string|array
		 */
		private static $quotes = <<<TXT
Be not ashamed of mistakes and thus make them crimes.
Before you embark on a journey of revenge, dig two graves.
Everything has its beauty but not everyone sees it.
Forget injuries, never forget kindnesses.
He who will not economize will have to agonize.
I hear and I forget. I see and I remember. I do and I understand.
Ignorance is the night of the mind, but a night without moon and star.
It does not matter how slowly you go so long as you do not stop.
Men's natures are alike, it is their habits that carry them far apart.
Our greatest glory is not in never falling, but in getting up every time we do.
Respect yourself and others will respect you.
Study the past if you would define the future.
The superior man, when resting in safety, does not forget that danger may come. When in a state of security he does not forget the possibility of ruin. When all is orderly, he does not forget that disorder may come. Thus his person is not endangered, and his States and all their clans are preserved.
To be able under all circumstances to practice five things constitutes perfect virtue; these five things are gravity, generosity of soul, sincerity, earnestness and kindness.
To see what is right and not to do it is want of courage.
To see what is right, and not to do it, is want of courage or of principle.
What the superior man seeks is in himself; what the small man seeks is in others.
When anger rises, think of the consequences.
When we see men of a contrary character, we should turn inwards and examine ourselves.
Wheresoever you go, go with all your heart.
They must often change who would be constant in happiness or wisdom.
By nature, men are nearly alike; by practice, they get to be wide apart.
Fine words and an insinuating appearance are seldom associated with true virtue.
Have no friends not equal to yourself.
He who exercises government by means of his virtue may be compared to the north polar star, which keeps its place and all the stars turn towards it.
He who speaks without modesty will find it difficult to make his words good.
He with whom neither slander that gradually soaks into the mind, nor statements that startle like a wound in the flesh, are successful may be called intelligent indeed.
Hold faithfulness and sincerity as first principles.
I am not one who was born in the possession of knowledge; I am one who is fond of antiquity, and earnest in seeking it there.
I have not seen a person who loved virtue, or one who hated what was not virtuous. He who loved virtue would esteem nothing above it.
TXT;

		/**
		 * Prepare all quotes
		 *
		 * @static
		 */
		protected static function prepare(){
			if(is_string(self::$quotes)){
				self::$quotes = explode("\r",self::$quotes);
			}
		}

		/**
		 * Return a random Confucius quote
		 *
		 * @static
		 * @return string
		 */
		public static function said(){
			self::prepare();
			return "Confucius said: <em>&ldquo;" . trim(self::$quotes[rand(0,count(self::$quotes)-1)],50) . "&rdquo;</em>";
		}
	}
