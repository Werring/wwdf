<?php
	/**
	 * Loads POST and GET data
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Model
	 * @subpackage Request
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class Model_Request extends Model_Base {
		const POST = true;
		const GET  = false;

		/**
		 * Request type (POST | GET)
		 *
		 * @var string
		 * @static
		 * @access private
		 */
		private static $type;
		/**
		 * GET and / or POST data
		 *
		 * @var array
		 *
		 * @access private
		 */
		private $data;

		/**
		 * Constructor
		 *
		 * @access public
		 */
		public function __construct(){
			self::$type = strval($_SERVER['REQUEST_METHOD']);
			$this->data = $_POST;
			foreach($_GET as $key => $value){
				$prefix = "";
				if(isset($this->data[$key])) $prefix = "_GET";
				$this->data[$prefix . $key] = $value;
			}
		}

		/**
		 * Get current type or check if given type is the current type
		 *
		 * @static
		 *
		 * @param null|string $type
		 *
		 * @access public
		 * @return bool|string
		 */
		public static function type($type = null){
			if(is_null($type))
				$ret = self::$type;
			else{
				$ret = (strtoupper($type) == self::$type);
			}
			return $ret;
		}

		/**
		 * Getter for GET|POST request
		 *
		 * @access public
		 *
		 * @param      $key
		 * @param bool $usePost
		 *
		 * @return RequestData
		 */
		public function get($key, $usePost = true){
			if(self::$type == 'POST' && $usePost){
				$data        = (isset($_POST[$key])) ? $_POST[$key] : null;
				$requestData = new RequestData($data);
			}
			else{
				$data        = (isset($_GET[$key])) ? $_GET[$key] : null;
				$requestData = new RequestData($data);
			}
			return $requestData;
		}

		/**
		 * Var_export with all RequestData from $data
		 *
		 * @access public
		 *
		 * @return string
		 */
		public function __toString(){
			$request = array();

			foreach($this->data as $key => $value){
				$request[$key] = $this->get($key);
			}
			return var_export($request, true);
		}

	}

	/**
	 * Request Data object
	 *
	 * @author     werring <info@werringweb.nl>
	 * @package    Model
	 * @subpackage Request
	 * @copyright  2009-2012 Werring webdevelopment
	 */
	class RequestData {
		/**
		 * Request data
		 *
		 * @access private
		 * @var mixed
		 */
		private $data;

		/**
		 * Constructor
		 *
		 * @access public
		 *
		 * @param mixed $data
		 */
		public function __construct($data){
			$this->data = $data;
		}

		/**
		 * Strval $data
		 *
		 * @access public
		 * @return string
		 */
		public function __toString(){
			return strval($this->data);
		}

		/**
		 * Validate data as email
		 *
		 * @access public
		 * @return bool|string
		 */
		public function asEmail(){
			$ret = false;
			if(Model_Mail::validateMail($this->data, true)){
				$ret = $this->data;
			}
			return $ret;
		}

		/**
		 * Validate as numeric and return int value
		 *
		 * @access public
		 * @return int
		 */
		public function asInt(){
			$ret = 0;
			if(is_numeric($this->data)) $ret = intval($this->data);
			return $ret;
		}

		/**
		 * Validate as numeric and return numeric value
		 *
		 * @access public
		 * @return float
		 */
		public function asNumber(){
			$ret = 0;
			if(is_numeric($this->data)) $ret = floatval($this->data);
			return (float) $ret;
		}

		/**
		 * Return value as string
		 *
		 * @access public
		 * @return string
		 */
		public function asString(){
			return strval($this->data);
		}

		/**
		 * Return value as array
		 *
		 * If value isn't an array, it will return an
		 * array with as first value the original value
		 *
		 * @access public
		 * @return array
		 */
		public function asArray(){
			if(is_array($this->data)){
				$ret = $this->data;
			}
			else{
				$ret = array($this->data);
			}
			return $ret;
		}

		/**
		 * Returns the boolean value of the data,
		 *
		 * If it is a string, return true only on:
		 *
		 * * 1
		 * * t
		 * * true
		 * * y
		 * * yes
		 * * ok
		 * * accept
		 * * confirm
		 * * allow
		 *
		 * Other strings return a false
		 *
		 * @access public
		 * @return bool
		 */
		public function asBool(){
			if(is_string($this->data)){
				switch(strtolower($this->data)){
					case '1':
					case 't':
					case 'true':
					case 'y':
					case 'yes':
					case 'ok':
					case 'accept':
					case 'confirm':
					case 'allow':
						$ret = true;
						break;
					default:
						$ret = false;
						break;
				}
			}
			else{
				$ret = (bool) $this->data;
			}
			return $ret;
		}

		/**
		 * Return raw and unchecked data
		 *
		 * @access public
		 * @return mixed
		 */
		public function asRaw(){
			return $this->data;
		}
	}