<?php
	/**
	 * Checks database for given page
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Model
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	class Model_DatabasePage extends Model_Base {
		/**
		 * URL of page
		 *
		 * @access protected
		 * @var string $page
		 */
		protected $page;
		/**
		 * Content of page
		 *
		 * @access public
		 * @var string|null
		 */
		public $content = null;
		/**
		 * Title of page
		 *
		 * @access public
		 * @var null|string
		 */
		public $title = null;
		/**
		 * Do we need to be logged in to view this page
		 *
		 * @access public
		 * @var bool
		 */
		public $logginRequired = false;

		/**
		 * Constructor
		 *
		 * @access public
		 *
		 * @param string $page URL of page
		 */
		public function __construct($page){
			$this->initModel();
			$this->getPageDetails($page);
		}

		/**
		 * Get page data from database
		 *
		 * If page not found, redirect to a 404 page
		 *
		 * @see Controller_Page_404::create404
		 *
		 * @param string $page
		 */
		protected function getPageDetails($page){
			$pages   = $this->reg->DB->prefixTable('pages');
			$content = $this->reg->DB->prefixTable('content');
			$sql     = <<<SQL
INNER JOIN {$content}
ON {$pages}.contentID = {$content}.id
WHERE {$pages}.path = "{$page}"
SQL;
			$result  = $this->reg->DB->select('pages', '*', $sql);
			if($result['affected'] == 0){
				Controller_Page_404::create404($this->reg->Dispatch->page);
			}
			$this->page           = $page;
			$this->content        = $result[0]['content'];
			$this->title          = $result[0]['title'];
			$this->logginRequired = (bool) $result[0]['loginRequired'];
		}

		/**
		 * Does current user has access to this page?
		 *
		 * @access public
		 * @return bool
		 */
		public function hasAccess(){
			$access = false;
			if(($this->logginRequired && $this->reg->User->loggedIn) || !$this->logginRequired){
				$access = true;
			}
			return $access;
		}
	}
