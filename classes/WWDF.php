<?php
	/**
	 * WWDF
	 *
	 * Werring Webdevelopment Framework
	 * Based on MVC model
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Core
	 * @copyright 2009-2012 Werring webdevelopment
	 */
	final class WWDF {
		/**
		 * Link to Registry
		 *
		 * @access private
		 * @var $reg Registry
		 */
		private $reg;

		/**
		 * Constructor
		 *
		 * @access public
		 *         define all registery content
		 */
		public function __construct(){
			$this->reg           = Registry::getInstance();
			$this->reg->Request  = new Model_Request();
			$this->reg->Conf     = new Model_Configuration();
			$this->reg->DB       = new Model_Database();
			$this->reg->JSON     = new View_JSON();
			$this->reg->Template = new View_Template($this->reg->Conf->template);
			$this->reg->Dispatch = new Dispatcher();
			$this->reg->User     = new Model_User();
			$this->reg->Headers  = new Model_Header();
			$this->reg->Menu     = new Model_Menu();
		}

		/**
		 * write logs after die / exit
		 *
		 * @access public
		 */
		public function __destruct(){
			Model_Logfile::writeLog("CallerIP",$_SERVER['REMOTE_ADDR']);
			Model_Logfile::finalizeLogs();
		}

		/**
		 * init framework
		 *
		 * @access public
		 */
		public function init(){
			$page = ($this->reg->Request->get('page', Model_Request::GET)->asString());
			$this->reg->DB->connect();
			$this->reg->User->init();
			$this->reg->Template->setVar('sitename', $this->reg->Conf->sitename);
			$this->reg->Dispatch->handle($page);
			$this->reg->Template->parse();

		}
	}
