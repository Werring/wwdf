<?php
	/**
	 * Config file
	 *
	 * @author    werring <info@werringweb.nl>
	 * @package   Core
	 * @copyright 2009-2012 Werring webdevelopment
	 */

	$__ = array(); #Keep this line here
	#############################
	# Settings                  #
	# Uncomment lines if needed #
	#############################
	$__['template']           = 'default'; # Template name
	$__['sitename']           = 'Werringweb Framework'; # Site name
	$__['database']['host']   = 'localhost'; # mysql Hostname
	$__['database']['user']   = 'wwdf'; # mysql Username
	$__['database']['pass']   = ''; # mysql Password
	$__['database']['daba']   = 'wwdf'; # mysql Database
	$__['database']['prefix'] = 'framework_'; # mysql Database
	$__['md5Integrity']       = 'b5939c665bfbeed297830a76b88feca4'; #md5 for /index.php
	$__['admin']              = 'admin@example.com';
	$__['errorLevel']['file'] = 10; # 10 = ALL, 0 = none;
	$__['errorLevel']['mail'] = 10; # 10 = ALL, 0 = none;